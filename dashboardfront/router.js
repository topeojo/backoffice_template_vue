import Vue from "vue";
import Router from "vue-router";
import DashboardLayout from "@/layout/DashboardLayout";
import AuthLayout from "@/layout/AuthLayout";

import Login from "@/views/Login";
import Dashboard from "@/views/Dashboard";
import Profile from "@/views/UserProfile";
import Tables from "@/views/Tables";
import CreateUser from "@/views/CreateUser";
import RolePriviledges from "@/views/RolePriviledges";
import GenerateReport from "@/views/GenerateReport";
import GenerateReport1 from "@/views/GenerateReport1";

Vue.use(Router);

let router = new Router({
  linkExactActiveClass: "active",
  mode: "history",
  routes: [
    {
      path: "/",
      redirect: "login",
      component: AuthLayout,
      children: [
        {
          path: "/login",
          name: "login",
          component: Login
        }
      ]
    },
    {
      path: "/board",
      redirect: "dashboard",
      component: DashboardLayout,
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: "/dashboard",
          name: "dashboard",
          component: Dashboard
        },
        {
          path: "/profile",
          name: "profile",
          component: Profile
        },
        {
          path: "/tables",
          name: "tables",
          component: Tables
        },
        {
          path: "/createUser",
          name: "create new user",
          component: CreateUser
        },
        {
          path: "/rolePriviledges",
          name: "Setup Role Priviledges",
          component: RolePriviledges
        },
        {
          path: "/generateReport",
          name: "Generate Report Using Redis String",
          component: GenerateReport
        },
        {
          path: "/generateReport1",
          name: "Generate Report Using Redis List",
          component: GenerateReport1
        }
      ]
    }
  ]

  // routes: [
  //   {
  //     path: '/',
  //     redirect: 'dashboard',
  //     component: DashboardLayout,
  //     children: [
  //       {
  //         path: '/dashboard',
  //         name: 'dashboard',
  //         // route level code-splitting
  //         // this generates a separate chunk (about.[hash].js) for this route
  //         // which is lazy-loaded when the route is visited.
  //         component: () => import(/* webpackChunkName: "demo" */ './views/Dashboard.vue')
  //       },
  //       {
  //         path: '/icons',
  //         name: 'icons',
  //         component: () => import(/* webpackChunkName: "demo" */ './views/Icons.vue')
  //       },
  //       {
  //         path: '/profile',
  //         name: 'profile',
  //         component: () => import(/* webpackChunkName: "demo" */ './views/UserProfile.vue')
  //       },
  //       {
  //         path: '/maps',
  //         name: 'maps',
  //         component: () => import(/* webpackChunkName: "demo" */ './views/Maps.vue')
  //       },
  //       {
  //         path: '/tables',
  //         name: 'tables',
  //         component: () => import(/* webpackChunkName: "demo" */ './views/Tables.vue')
  //       }
  //     ]
  //   },
  //   {
  //     path: '/',
  //     redirect: 'login',
  //     component: AuthLayout,
  //     children: [
  //       {
  //         path: '/login',
  //         name: 'login',
  //         component: () => import(/* webpackChunkName: "demo" */ './views/Login.vue')
  //       },
  //       {
  //         path: '/register',
  //         name: 'register',
  //         component: () => import(/* webpackChunkName: "demo" */ './views/Register.vue')
  //       }
  //     ]
  //   }
  // ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem("jwt") == null) {
      next({
        path: "/login"
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
