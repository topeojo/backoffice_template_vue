/* eslint-disable */
import Vue from "vue";
import App from "./App.vue";
import router from "../router";
import "./registerServiceWorker";
import ArgonDashboard from "./plugins/argon-dashboard";
import axios from "axios";
import moment from "moment";

Vue.config.productionTip = false;
Vue.prototype.$http = axios;
Vue.prototype.$moment = moment;

Vue.mixin({
  data: function() {
    return {
      globalUserPriviledges: ""
    };
  },
  methods: {
    globalSetupUsersPriviledge() {
      this.$http
        .get("http://localhost:3000/getRolePriviledges")
        .then(response => {
          let data = response.data;
          let userRoleId = JSON.parse(localStorage.getItem("user")).userRole;
          this.globalUserPriviledges = data.filter(item => {
            return item.roleId == userRoleId;
          });
          console.log("View User priviledges");
          console.log(this.globalUserPriviledges);
        })
        .catch(err => {
          console.error(err);
        });
    },
    camelize(str) {
      return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function(match, index) {
        if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
        return index == 0 ? match.toLowerCase() : match.toUpperCase();
      });
    }
  }
});

Vue.use(ArgonDashboard);
new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
