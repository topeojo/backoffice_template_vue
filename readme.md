# Backoffice Template Built Using VueJS

This is a quick readme on how to use this template, although this readme will be updated from time to time, so do endeavour to pull👊 for updates.

This project currently consists of the Vue frontend and the backend which includes apis to a MySql database and Redis database.
***
To use this template, please ensure that your node is of version **10.14.** and above, and you have a local mysql and redis server installed on your PC.

### Alright, let's get into the fun part.
1. After pulling this project, run `npm install` on both the dashboardfront and server directories respectively to install the node dependencies.
2. The mysql database migration file has now been added to the repository, and you can import the sql file into your local mysql server.
3. Lastly on databases, start your redis server from your CLI. 
4. Next up, you have to setup your .env file for this project, please add these variables to your .env 

    * PORT = 3000
    * SECRET = *Any string value of your choice, this is used to generate tokens on login success*
    * HOST = "localhost"
    * USER = *Your mysql db username*
    * PASSWORD = *Your mysql db password. Just in case you have no password, still create this variable and equate it to an empty string like this ""*
    * DATABASE = "vue_dashboard"
  
5. To start up the application, `cd` into the server directory, run `npm start` to start the server.
6. Then `cd` into the dashboardfront directory and run `npm start` to start the vue local server.
7. From the `user` table in the mysql database, you can find test credentials to log into the system with.
   
***
Ok, that's all for now.   
We will keep updating this project.