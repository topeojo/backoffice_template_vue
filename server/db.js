"use strict";

// const sqlite3 = require('sqlite3').verbose()
const mysql = require("mysql");
// const config = require("./config");
require("dotenv").config;

class DB {
  constructor() {
    this.con = this.connectToDb();
  }

  connectToDb() {
    let con = mysql.createConnection({
      host: process.env.HOST,
      user: process.env.USER,
      password: process.env.PASSWORD,
      database: process.env.DATABASE
    });

    con.connect(err => {
      if (err) throw err;
      console.log("Server connected to mySQL DB");
      // this.createTable(con);
    });

    return con;
  }

  insertUser(user, callback) {
    let sql =
      "INSERT INTO user (name, email, userPass, userRole) VALUES (?,?,?,?)";
    return this.con.query(sql, user, err => {
      callback(err);
    });
  }

  insertIntoRolesPriviledges(values, callback) {
    let sql =
      "INSERT INTO rolepriviledges(roleId, priviledgeId, status, createdAt, updatedAt) VALUES (?,?,?,?,?)";
    return this.con.query(sql, values, err => {
      callback(err);
    });
  }

  insertNewRole(values, callback) {
    let sql = "insert into roles(roleName) values (?)";
    return this.con.query(sql, values, err => {
      callback(err);
    });
  }

  insertNewPriviledge(values, callback) {
    let sql = "insert into priviledges(priviledge) values (?)";
    return this.con.query(sql, values, err => {
      callback(err);
    });
  }

  insertUserProfile(values, callback) {
    let sql =
      "insert into userprofile(userId, userName, email, firstName, lastName, address, city, country, zipCode, about) values (?,?,?,?,?,?,?,?,?,?)";
    return this.con.query(sql, values, err => {
      callback(err);
    });
  }

  updateRolePriviledge(values, callback) {
    let sql = `update rolepriviledges set status = ?, updatedAt = ? where roleId = ? and priviledgeId = ?`;
    return this.con.query(sql, values, err => {
      callback(err);
    });
  }

  updateUserProfile(values, callback) {
    let sql = `update userprofile set userId = ?, userName = ?, email = ?, firstName = ?, lastName = ?, address = ?, city = ?, country = ?, zipCode = ?, about = ? where userId = ?`;
    return this.con.query(sql, values, err => {
      callback(err);
    });
  }

  selectByEmail(email, callback) {
    return this.con.query(
      `SELECT * FROM user WHERE email = ?`,
      [email],
      function(err, row) {
        callback(err, row);
      }
    );
  }

  getDashBoardMenus(callback) {
    let sql = "select * from dashboardmenus order by id";
    return this.con.query(sql, (err, row) => {
      callback(err, row);
    });
  }

  getRoles(callback) {
    let sql = "select id,roleName from roles order by id";
    return this.con.query(sql, (err, row) => {
      callback(err, row);
    });
  }

  getPriviledges(callback) {
    let sql = "select id,priviledge from priviledges order by id";
    return this.con.query(sql, (err, row) => {
      callback(err, row);
    });
  }

  getRolePriviledges(callback) {
    let sql = `select r.roleName, rp.roleId, rp.priviledgeId, rp.status, p.priviledge
    from rolepriviledges rp
    left join priviledges p on rp.priviledgeId = p.id
    left join roles r on rp.roleId = r.id order by rp.roleId `;

    return this.con.query(sql, (err, row) => {
      callback(err, row);
    });
  }

  getUserProfile(values, callback) {
    let sql = `select * from userprofile where userId = ?`;
    return this.con.query(sql, [values], (err, row) => {
      callback(err, row);
    });
  }
}

module.exports = DB;
